# starter_intro_2022_12_12_gr1212

---

### Важливо: пропоную встановлювати PyCharm не Community Edition, a Professional - якщо ви плануєте навчатись у CyberBionics. Місяць професіональна версія працю безкоштовно, а за цей час вам нададуть безкоштовний код активації. Нам пізніше будуть необхідні деякі можливості професійної версії. 
[video - встановлення python + встановлення PyCharm](https://www.youtube.com/watch?v=BjmW8QQZ8Bg&t)

[video - записане раніше](https://www.youtube.com/watch?v=asF2Hg2ZOkI&list=PLOlyZEVllXBHso6SasmoJdNWVO63573RN)

[video - наша лекція](https://www.youtube.com/watch?v=8mnALf8DyP8)

